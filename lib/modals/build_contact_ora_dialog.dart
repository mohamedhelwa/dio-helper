import 'package:dio_helper/ModaLs/LoadingDialog.dart';
import 'package:dio_helper/utils/DioUtils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_phone_direct_caller/flutter_phone_direct_caller.dart';

class BuildContactOraDialog extends StatelessWidget {
  const BuildContactOraDialog({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(15))),
      insetPadding: const EdgeInsets.all(0),
      contentPadding: const EdgeInsets.all(0),
      content: Stack(
        children: [
          Container(
            constraints: BoxConstraints(
              maxHeight: 330,
              minWidth: MediaQuery.of(context).size.width * 0.85,
              maxWidth: MediaQuery.of(context).size.width * 0.85,
            ),
            alignment: Alignment.topCenter,
            padding: const EdgeInsets.all(15).copyWith(top: 20),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: const BorderRadius.all(Radius.circular(15)),
            ),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Text(
                  DioUtils.lang == "en"
                      ? "Please Contact ORA"
                      : "من فضلك قم بالإتصال بـ ORA",
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 30),
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 25),
                        child: Text(
                          DioUtils.lang == "en"
                              ? "Contact ORA support team on:"
                              : "اتصل بفريق دعم ORA علي:",
                        ),
                      ),
                      InkWell(
                        onTap: () async {
                          bool? res = await FlutterPhoneDirectCaller.callNumber(
                              "15003");
                          if (res == null) {
                            CustomToast.showToastNotification(
                                "Something Went Wrong");
                          }
                        },
                        child: Directionality(
                          textDirection: TextDirection.ltr,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Icon(Icons.call, color: Colors.grey),
                              const SizedBox(width: 8),
                              Text(
                                "15003",
                                style: TextStyle(
                                  color: Colors.grey,
                                  fontWeight: FontWeight.bold,
                                  decoration: TextDecoration.underline,
                                  fontSize: 20,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 10),
                        child: Text(DioUtils.lang == "en" ? "Or" : "أو"),
                      ),
                      InkWell(
                        onTap: () async {
                          bool? res = await FlutterPhoneDirectCaller.callNumber(
                              "+20221267500");
                          if (res == null) {
                            CustomToast.showToastNotification(
                                "Something Went Wrong");
                          }
                        },
                        child: Directionality(
                          textDirection: TextDirection.ltr,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Icon(
                                Icons.call,
                                color: Colors.grey,
                              ),
                              const SizedBox(width: 8),
                              Text(
                                "+20221267500",
                                style: TextStyle(
                                  color: Colors.grey,
                                  fontWeight: FontWeight.bold,
                                  decoration: TextDecoration.underline,
                                  fontSize: 20,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Positioned(
            top: 0,
            right: DioUtils.lang == "ar" ? 0 : null,
            left: DioUtils.lang == "ar" ? null : 0,
            child: IconButton(
              onPressed: () => Navigator.of(context).pop(),
              padding: const EdgeInsets.all(0),
              splashRadius: 20,
              icon: Icon(
                Icons.close,
                size: 30,
                color: Colors.black,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
