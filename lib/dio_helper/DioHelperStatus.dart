part of 'DioImports.dart';

class DioHelper {
  late Dio _dio;
  late DioCacheManager _manager;
  BuildContext context;
  final bool forceRefresh;
  static late String baseUrl;

  bool _isRefreshing = false;
  Completer<void> _completer = Completer<void>();

  static init({required String base}) {
    baseUrl = base;
  }

  DioHelper({this.forceRefresh = true, required this.context}) {
    _dio = Dio(
      BaseOptions(
        baseUrl: baseUrl,
        // contentType: "application/x-www-form-urlencoded; charset=utf-8",
      ),
    )
      ..interceptors.add(_getCacheManager().interceptor)
      ..interceptors.add(
        LogInterceptor(
          responseBody: true,
          requestBody: true,
          logPrint: (data) => myLog(data.toString()),
        ),
      )
      ..interceptors.add(
        CertificatePinningInterceptor(
          allowedSHAFingerprints: [
            "AF:03:03:EB:12:A5:F6:62:92:E3:4C:2A:DD:30:F8:9E:86:47:D0:EA:05:E7:D2:04:BD:CE:45:DD:B8:CB:90:29"
          ],
        ),
      );
    _completer.complete();
  }

  DioCacheManager _getCacheManager() {
    _manager = DioCacheManager(
        CacheConfig(baseUrl: baseUrl, defaultRequestMethod: "POST"));
    return _manager;
  }

  Options _buildCacheOptions() {
    return buildCacheOptions(
      Duration(days: 3),
      maxStale: Duration(days: 7),
      forceRefresh: forceRefresh,
      options: Options(extra: {}),
    );
  }

  Future<dynamic> get({required String url}) async {
    _dio.options.headers = DioUtils.header ?? await _getHeader();
    try {
      var response = await _dio.get("$url", options: _buildCacheOptions());
      myPrint("response ${response.statusCode}");
      if (response.statusCode! > 199 || response.statusCode! < 400) {
        return response.data;
      } else {
        return showErrorMessage(response, retry: () async {
          _dio.options.headers = _getHeader();
          var response = await _dio.get("$url", options: _buildCacheOptions());
          myPrint("response ${response.statusCode}");
          return response.data;
        });
      }
    } on DioError catch (e) {
      return showErrorMessage(e.response, retry: () async {
        _dio.options.headers = _getHeader();
        var response = await _dio.get("$url", options: _buildCacheOptions());
        myPrint("response ${response.statusCode}");
        return response.data;
      });
    }
  }

  Future<dynamic> post({
    required String url,
    required Map<String, dynamic> body,
    bool showLoader = true,
    bool showErrorMsg = true,
  }) async {
    if (showLoader) DioUtils.showLoadingDialog();
    _printRequestBody(body);
    FormData formData = FormData.fromMap(body);
    bool haveFile = false;
    body.forEach((key, value) async {
      if ((value) is File) {
        haveFile = true;
        //create multipart using filepath, string or bytes
        MapEntry<String, MultipartFile> pic = MapEntry(
          "$key",
          MultipartFile.fromFileSync(value.path,
              filename: value.path.split("/").last),
        );
        //add multipart to request
        formData.files.add(pic);
      } else if ((value) is List<File>) {
        haveFile = true;
        List<MapEntry<String, MultipartFile>> files = [];
        value.forEach((element) async {
          MapEntry<String, MultipartFile> pic = MapEntry(
              "$key",
              MultipartFile.fromFileSync(
                element.path,
                filename: element.path.split("/").last,
              ));
          files.add(pic);
        });
        formData.files.addAll(files);
      }
    });

    _dio.options.headers = DioUtils.header ?? await _getHeader();
    //create multipart request for POST or PATCH method

    try {
      var response = await _dio.post("$url", data: haveFile ? formData : body);
      myPrint("response ${response.statusCode}");
      if (showLoader) DioUtils.dismissDialog();
      if (response.statusCode == 200 || response.statusCode == 201) {
        if (showErrorMsg) {
          if (response.data["message"] != null) {
            showErrorMessage(response);
          }
        }

        return response.data;
      } else {
        showErrorMessage(response);
      }
    } on DioError catch (e) {
      if (showLoader) DioUtils.dismissDialog();
      showErrorMessage(e.response);
    }

    return null;
  }

  Future<dynamic> put({
    required String url,
    required Map<String, dynamic> body,
    bool showLoader = true,
    bool showErrorMsg = true,
  }) async {
    if (showLoader) DioUtils.showLoadingDialog();
    _printRequestBody(body);
    FormData formData = FormData.fromMap(body);
    bool haveFile = false;
    body.forEach((key, value) async {
      if ((value) is File) {
        haveFile = true;
        //create multipart using filepath, string or bytes
        MapEntry<String, MultipartFile> pic = MapEntry(
          "$key",
          MultipartFile.fromFileSync(value.path,
              filename: value.path.split("/").last),
        );
        //add multipart to request
        formData.files.add(pic);
      } else if ((value) is List<File>) {
        haveFile = true;
        List<MapEntry<String, MultipartFile>> files = [];
        value.forEach((element) async {
          MapEntry<String, MultipartFile> pic = MapEntry(
              "$key",
              MultipartFile.fromFileSync(
                element.path,
                filename: element.path.split("/").last,
              ));
          files.add(pic);
        });
        formData.files.addAll(files);
      }
    });

    _dio.options.headers = DioUtils.header ?? await _getHeader();
    //create multipart request for POST or PATCH method

    try {
      var response = await _dio.put("$url", data: haveFile ? formData : body);
      myPrint("response ${response.statusCode}");
      if (showLoader) DioUtils.dismissDialog();
      if (response.statusCode == 200 || response.statusCode == 201) {
        if (showErrorMsg) {
          if (response.data["message"] != null) {
            showErrorMessage(response);
          }
        }
        return response.data;
      } else {
        showErrorMessage(response);
      }
    } on DioError catch (e) {
      if (showLoader) DioUtils.dismissDialog();
      showErrorMessage(e.response);
    }

    return null;
  }

  Future<dynamic> patch({
    required String url,
    required Map<String, dynamic> body,
    bool showLoader = true,
    bool showErrorMsg = true,
  }) async {
    if (showLoader) DioUtils.showLoadingDialog();
    _printRequestBody(body);
    _dio.options.headers = DioUtils.header ?? await _getHeader();
    try {
      var response = await _dio.patch("$url", data: body);
      myPrint("response ${response.statusCode}");
      if (showLoader) DioUtils.dismissDialog();
      if (response.statusCode == 200 || response.statusCode == 201) {
        if (showErrorMsg) {
          if (response.data["message"] != null) {
            showErrorMessage(response);
          }
        }
        return response.data;
      } else {
        showErrorMessage(response);
      }
    } on DioError catch (e) {
      if (showLoader) DioUtils.dismissDialog();
      showErrorMessage(e.response);
    }

    return null;
  }

  Future<dynamic> delete({
    required String url,
    required Map<String, dynamic> body,
    bool showLoader = true,
    bool showErrorMsg = true,
  }) async {
    if (showLoader) DioUtils.showLoadingDialog();
    _printRequestBody(body);
    _dio.options.headers = DioUtils.header ?? await _getHeader();
    try {
      var response = await _dio.delete("$url", data: body);
      myPrint("body response ${response.statusCode}");
      if (showLoader) DioUtils.dismissDialog();
      if (response.statusCode == 200 || response.statusCode == 201) {
        if (showErrorMsg) {
          if (response.data["message"] != null) {
            showErrorMessage(response);
          }
        }
        return response.data;
      } else {
        showErrorMessage(response);
      }
    } on DioError catch (e) {
      if (showLoader) DioUtils.dismissDialog();
      showErrorMessage(e.response);
    }

    return null;
  }

  Future<dynamic> uploadChatFile(
    String url,
    Map<String, dynamic> body, {
    bool showLoader = true,
    bool showErrorMsg = true,
  }) async {
    if (showLoader) DioUtils.showLoadingDialog();
    _printRequestBody(body);
    FormData formData = FormData.fromMap(body);
    body.forEach((key, value) async {
      if ((value) is File) {
        //create multipart using filepath, string or bytes
        MapEntry<String, MultipartFile> pic = MapEntry(
          "$key",
          MultipartFile.fromFileSync(value.path,
              filename: value.path.split("/").last),
        );
        //add multipart to request
        formData.files.add(pic);
      } else if ((value) is List<File>) {
        List<MapEntry<String, MultipartFile>> files = [];
        value.forEach((element) async {
          MapEntry<String, MultipartFile> pic = MapEntry(
              "$key",
              MultipartFile.fromFileSync(
                element.path,
                filename: element.path.split("/").last,
              ));
          files.add(pic);
        });
        formData.files.addAll(files);
      } else {
        // requestData.addAll({"$key":"$value"});

      }
    });

    _dio.options.headers = DioUtils.header ?? await _getHeader();
    //create multipart request for POST or PATCH method

    try {
      var response = await _dio.post("$url", data: formData);
      myPrint("response ${response.statusCode}");
      if (showLoader) DioUtils.dismissDialog();
      if (response.statusCode == 200 || response.statusCode == 201) {
        if (showErrorMsg) {
          if (response.data["message"] != null) {
            showErrorMessage(response);
          }
        }
        return response.data;
      } else {
        showErrorMessage(response);
      }
    } on DioError catch (e) {
      if (showLoader) DioUtils.dismissDialog();
      showErrorMessage(e.response);
    }

    return null;
  }

  void _printRequestBody(Map<String, dynamic> body) {
    myPrint(
        "-------------------------------------------------------------------");
    myLog("$body");
    myPrint(
        "-------------------------------------------------------------------");
  }

  showErrorMessage(Response? response, {Function()? retry}) async {
    if (response == null) {
      myPrint("Failed response Check Server");
      CustomToast.showToastNotification("Check your internet connection!");
    } else {
      myPrint("Failed response ${response.statusCode}");
      myPrint("Failed response ${response.data}");
      var data = response.data;
      if (data is String) data = json.decode(response.data);
      switch (response.statusCode) {
        case 201:
          CustomToast.showToastNotification(data["message"].toString());
          break;
        case 500:
          CustomToast.showToastNotification(data["message"].toString());
          break;
        case 404:
          CustomToast.showToastNotification(data["message"].toString());
          break;
        case 401:
          CustomToast.showToastNotification(data["message"].toString());
          await refreshToken();
          if (retry != null) {
            myPrint("retry");
            return await retry();
          }
          break;
        case 403:
          CustomToast.showToastNotification(data["message"].toString());
          await refreshToken();
          if (retry != null) {
            return await retry();
          }
          break;
        case 408:
          CustomToast.showToastNotification(data["message"].toString());
          Future.delayed(const Duration(seconds: 2), () async {
            EncryptedSharedPreferences prefs = EncryptedSharedPreferences();
            await prefs.clear();
            Phoenix.rebirth(context);
          });
          break;
        case 411:
          CustomToast.showToastNotification(data["message"].toString());
          Future.delayed(const Duration(seconds: 1), () async {
            CustomToast.showContactOraDialog(context);
          });
          break;
        case 400:
          if (data["errors"] != null) {
            Map<String, dynamic> errors = data["errors"];
            myPrint("response errors $errors");
            errors.forEach((key, value) {
              List<String> lst = List<String>.from(value.map((e) => e));
              lst.forEach((e) {
                CustomToast.showToastNotification(e);
              });
            });
          } else {
            CustomToast.showToastNotification(data["message"].toString());
          }
          break;
        case 301:
        case 302:
          tokenExpired();
          break;
      }
    }
  }

  Future<void> refreshToken() async {
    myPrint("refreshToken");
    if (!_isRefreshing) {
      myPrint("Is not refreshing");
      _isRefreshing = true;
      _completer = Completer<void>();
      await getRefreshToken();
    }

    await _completer.future;
  }

  getRefreshToken() async {
    try {
      myPrint("getRefreshToken");

      _dio.options.headers = _getRefreshHeader();

      myLog("refreshToken ==> ${_dio.options.headers}");
      myPrint("URL ==> $baseUrl${"user/refresh"}");

      var response = await _dio.get("$baseUrl${"user/refresh"}");

      myPrint("response ${response.statusCode}");
      myPrint("getRefreshToken response ${response.statusCode}");

      if (response.statusCode == 200 || response.statusCode == 201) {
        GlobalState.instance.set("token", response.data["token"]);
        GlobalState.instance.set("refreshToken", response.data["refreshToken"]);

        _isRefreshing = false;
        _completer.complete();

        // return response.data["token"];
      } else {
        myPrint("getRefreshToken error ${response.statusCode}");
        // showErrorMessage(response);
      }
    } catch (e) {
      // showErrorMessage(e.response);
      myPrint("getRefreshToken error $e");
    }
  }

  _getHeader() {
    String? token = GlobalState.instance.get("token");
    String? refreshToken = GlobalState.instance.get("refreshToken");
    String? appCheckToken = GlobalState.instance.get("appCheckToken");
    String? entity = GlobalState.instance.get("entity");
    String? origin = GlobalState.instance.get("origin");
    String? device = Platform.isIOS ? "ios" : "android";
    String? lang = DioUtils.lang;
    String? version = GlobalState.instance.get("packageInfo").version;

    var headers = {
      'Accept': 'application/json',
      'Authorization': 'Bearer $token',
      'device': '$device',
      'version': '$version',
      'entity': '$entity',
      'lang': '$lang',
    };

    if (origin != null && origin != "") {
      headers.addAll({'origin': '$origin'});
    }

    if (appCheckToken != null && appCheckToken != "") {
      headers.addAll({'X-Firebase-AppCheck': '$appCheckToken'});
    }

    if (refreshToken != null && refreshToken != "") {
      headers.addAll({'refreshToken': '$refreshToken'});
    }

    return headers;
  }

  _getRefreshHeader() {
    String? token = GlobalState.instance.get("token");
    String? refreshToken = GlobalState.instance.get("refreshToken");
    String? appCheckToken = GlobalState.instance.get("appCheckToken");
    String? entity = GlobalState.instance.get("entity");
    String? origin = GlobalState.instance.get("origin");
    String? device = Platform.isIOS ? "ios" : "android";
    String? lang = DioUtils.lang;
    String? version = GlobalState.instance.get("packageInfo").version;

    var headers = {
      'Accept': 'application/json',
      'Authorization': 'Bearer $token',
      'device': '$device',
      'version': '$version',
      'entity': '$entity',
      'lang': '$lang',
    };

    if (origin != null && origin != "") {
      headers.addAll({'origin': '$origin'});
    }

    if (appCheckToken != null && appCheckToken != "") {
      headers.addAll({'X-Firebase-AppCheck': '$appCheckToken'});
    }

    if (refreshToken != null && refreshToken != "") {
      if (headers.containsKey("Authorization")) {
        headers.remove("Authorization");
        headers.addAll({'Authorization': '$refreshToken'});
      }
    }

    return headers;
  }

  void tokenExpired() async {
    EncryptedSharedPreferences prefs = EncryptedSharedPreferences();
    await prefs.clear();
    AutoRouter.of(context).popUntilRouteWithName(DioUtils.authRoute);
  }
}
