import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:auto_route/auto_route.dart';
import 'package:dio/dio.dart';
import 'package:dio_helper/ModaLs/LoadingDialog.dart';
import 'package:dio_helper/utils/DioUtils.dart';
import 'package:dio_helper/utils/GlobalState.dart';
import 'package:dio_helper/utils/debug_helper.dart';
import 'package:encrypted_shared_preferences/encrypted_shared_preferences.dart';
import 'package:flutter/material.dart';
import 'package:flutter_phoenix/flutter_phoenix.dart';
import 'package:http_certificate_pinning/http_certificate_pinning.dart';
import 'package:tf_dio_cache/dio_http_cache.dart';

part 'DioHelperStatus.dart';
part 'PrevDioHelper.dart';
