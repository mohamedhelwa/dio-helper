import 'dart:developer';

import 'package:flutter/foundation.dart';

myPrint(Object? object) {
  if (kDebugMode) print(object);
}

myLog(String? object) {
  if (kDebugMode) log("$object");
}
